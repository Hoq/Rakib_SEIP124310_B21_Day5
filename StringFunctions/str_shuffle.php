<?php
/**
str_shuffle — Randomly shuffles a string
 * Description
string str_shuffle ( string $str )
 *
 * str_shuffle() shuffles a string. One permutation of all possible is created.
 *
 *
 *
Parameters

str

The input string.

Return Values

Returns the shuffled string.

 */


$str = '123456';
$shuffled = str_shuffle($str);

// This will echo something like: 362154
echo $shuffled;
?>
