<?php
/**
implode — Join array elements with a string
 * Description
string implode ( string $glue , array $pieces )
 */




$a1 = array("10","20","30");
$b2 = array("a");
$c3 = array();

echo "a1 is: '".implode("','",$a1)."'<br>";
echo "b2 is: '".implode("','",$b2)."'<br>";
echo "c3 is: '".implode("','",$c3)."'<br>";

?>
