<?php
/* Logical Operators Example 	Name 	Result
$a and $b 	And 	TRUE if both $a and $b are TRUE.
$a or $b 	Or 	TRUE if either $a or $b is TRUE.
$a xor $b 	Xor 	TRUE if either $a or $b is TRUE, but not both.
! $a 	Not 	TRUE if $a is not TRUE.
$a && $b 	And 	TRUE if both $a and $b are TRUE.
$a || $b 	Or 	TRUE if either $a or $b is TRUE.

The reason for the two different variations of "and" and "or" operators is that they operate
at different precedences. (See Operator Precedence.)
 *
 *
 */

$x = 100;
$y = 50;

if ($x == 100 and $y == 50) {
    echo "HI!";
    echo "<br>";
}

echo "Example 1"."<hr>";

$x = 100;
$y = 50;

if ($x == 100 Or $y == 50) {
    echo "HI!";
    echo "<br>";
}

echo "Example 2"."<hr>";
?>
<?php
$x = 100;
$y = 50;

if ($x == 100 xor $y == 80) {
    echo "Hi!";
    echo "<br>";
}

echo "Example 3"."<hr>";
?>



<?php
$x = 100;
$y = 50;

if ($x == 100 && $y == 50) {
    echo "Nice World!";
    echo "<br>";
}
echo "Example 4"."<hr>";
?>

<?php
$x = 100;
$y = 50;

if ($x == 100 || $y == 80) {
    echo "Nice Tour!";
    echo "<br>";
}
echo "Example 5"."<hr>";
?>



<?php
$x = 100;

if ($x !== 90) {
    echo "Great!";
    echo "<br>";
}
echo "Example 6"."<hr>";
?>
