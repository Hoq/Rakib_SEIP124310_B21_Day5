<?php
/**
 *--$a 	Pre-decrement 	Decrements $a by one, then returns $a.
$a-- 	Post-decrement 	Returns $a, then decrements $a by one.
 */

echo "<h3>Example 1 Postdecrement</h3>";
$a = 10;
echo "Should be 10: " . $a-- . "<br />\n";
echo "Should be 9: " . $a . "<br />\n";

echo "<h3>Example 2 Predecrement</h3>";
$a = 10;
echo "Should be 9: " . --$a . "<br />\n";
echo "Should be 9: " . $a . "<br />\n";
?>