<?php
/**
ucwords — Uppercase the first character of each word in a string
 *

Description
string ucwords ( string $str [, string $delimiters = " \t\r\n\f\v" ] )

Returns a string with the first character of each word in str capitalized, if that character is alphabetic.

The definition of aword is any string of characters that is immediately after any character listed in the delimiters parameter (By default these are: space, form-feed, newline, carriage return, horizontal tab, and vertical tab).
Parameters

str

The input string.
delimiters

The optional delimiters contains the word separator characters.

Return Values

Returns the modified string.

 */


$foo = 'rakibul hoque';
$foo = ucwords($foo);


echo $foo;

?>
