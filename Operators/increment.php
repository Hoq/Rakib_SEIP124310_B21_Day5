<?php
/**
Increment Operators
 * ++$a 	Pre-increment 	Increments $a by one, then returns $a.
$a++ 	Post-increment 	Returns $a, then increments $a by one.
 */


echo "<h3>Example 1 Postincrement</h3>";
$a = 10;
echo "Should be 10: " . $a++ . "<br />\n";
echo "Should be 11: " . $a . "<br />\n";

echo "<h3>Example 2 Preincrement</h3>";
$a = 10;
echo "Should be 10: " . ++$a . "<br />\n";
echo "Should be 10: " . $a . "<br />\n";


?>