<?php
/**
 *addslashes — Quote string with slashes
 *
 * Description
string addslashes ( string $str )
 */

$str = "Is your name R'Rakib?";

// Outputs: Is your name R\'Rakib?
echo addslashes($str);