<?php
/**
Arithmetic Operators

Remember basic arithmetic from school? These work just like those.
Arithmetic Operators Example 	Name 	Result
-$a 	Negation 	Opposite of $a.
$a + $b 	Addition 	Sum of $a and $b.
$a - $b 	Subtraction 	Difference of $a and $b.
$a * $b 	Multiplication 	Product of $a and $b.
$a / $b 	Division 	Quotient of $a and $b.
$a % $b 	Modulus 	Remainder of $a divided by $b.
$a ** $b 	Exponentiation 	Result of raising $a to the $b'th power.
 */
//example 1  Negation 	Opposite of $a.
$a=10;
echo "Arithmetic Operators example 1  Negation 	Opposite of a=".-$a ;

echo "<hr>";
//example 2  Addition 	Sum of $a and $b
$a=10;
$b=10;
$c=$a+$b;
echo "Arithmetic Operatorsexample 2 Addition 	Sum of a and b=".$c ;

echo "<hr>";
//example 3  Subtraction 	Difference of $a and $b.
$a=20;
$b=10;
$c=$a-$b;
echo "Arithmetic Operators example 3 Subtraction 	Difference of a and b =".$c ;

echo "<hr>";
//example 4  Multiplication 	Product of $a and $b.
$a=20;
$b=10;
$c=$a*$b;
echo "Arithmetic Operators example 4 Multiplication 	Product of a and b =".$c ;

echo "<hr>";

//example 5  Division 	Quotient of $a and $b.
$a=20;
$b=10;
$c=$a/$b;
echo "Arithmetic Operatorsexample 5  Division 	Quotient of a and b =".$c ;

echo "<hr>";


//example 6  Modulus 	Remainder of $a divided by $b.
$a=20;
$b=10;
$c=$a%$b;
echo "Arithmetic Operators example 6  Modulus 	Remainder of a divided by b =".$c ;

echo "<hr>";

//example 7  Exponentiation 	Result of raising $a to the $b'th power.
$a = 2;
$b = 2;
$c = pow($a, $b);


echo "Arithmetic Operators example 7  Exponentiation 	Result of raising x to the y'th power=".$c ;

echo "<hr>";


?>