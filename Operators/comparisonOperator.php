<?php
/*Comparison Operators

Comparison operators, as their name implies, allow you to compare two values.
You may also be interested in viewing the type comparison tables, as
they show examples of various type related comparisons.

Comparison Operators Example 	Name 	Result
$a == $b 	Equal 	TRUE if $a is equal to $b after type juggling.
$a === $b 	Identical 	TRUE if $a is equal to $b, and they are of the same type.
$a != $b 	Not equal 	TRUE if $a is not equal to $b after type juggling.
$a <> $b 	Not equal 	TRUE if $a is not equal to $b after type juggling.
$a !== $b 	Not identical 	TRUE if $a is not equal to $b, or they are not of the same type.
$a < $b 	Less than 	TRUE if $a is strictly less than $b.
$a > $b 	Greater than 	TRUE if $a is strictly greater than $b.
$a <= $b 	Less than or equal to 	TRUE if $a is less than or equal to $b
*/
// example 1 Equal 	TRUE if $a is equal to $b after type juggling.
$a = 300;
$b = "300";

var_dump($a == $b);
echo "example 1 "."<hr>";
;
// example 2 Identical 	TRUE if $a is equal to $b, and they are of the same type
var_dump($a === $b);
echo "example 2 "."<hr>";

// example 3 Not equal 	TRUE if $a is not equal to $b after type juggling
var_dump($a !=  $b);
echo "example 3 "."<hr>";

// example 4 Not equal 	TRUE if $a is not equal to $b after type juggling
var_dump($a <>  $b);
echo "example 4 "."<hr>";

// example 5 Not identical 	TRUE if $a is not equal to $b, or they are not of the same type.
var_dump($a !==  $b);
echo "example 5 "."<hr>";
?>

<?php
// example 6 Less than 	TRUE if $a is strictly less than $b.
$a = 200;
$b = 300;


var_dump($a <  $b);
echo "example 6 "."<hr>";
?>

<?php
$a = 500;
$b = 300;

// example 7 Greater than 	TRUE if $a is strictly greater than $b.
var_dump($a >  $b);
echo "example 7 "."<hr>";
?>


